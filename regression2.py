#!/usr/bin/env python 

import pandas as pd
import statsmodels.formula.api as sm
from dgitcore import api 

api.initialize() 
repo = api.lookup('pingali', 'simple-regression-rawdata') 
r = repo.get_resource('demo-input.csv') 
df = pd.read_csv(r['localfullpath'])

result = sm.ols(formula="A ~ B + C", data=df).fit()

with open('model-results-api.txt', 'w') as fd: 
    fd.write(str(result.summary()))
